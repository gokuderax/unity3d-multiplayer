﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Capture : MonoBehaviour {

    public GameObject Dialog;
    public GameObject combatOptions;
    public GameObject captureImage;
    public GameObject temtem;
    private bool Animate;
    private combatDialogManager dMan;
    private actions action;
    //Cameras
    public GameObject CameraMain;
    public GameObject CombatCamera;

    void Start()
    {
        dMan = FindObjectOfType<combatDialogManager>();
        action = FindObjectOfType<actions>();
        Button btn1 = transform.GetComponent<Button>();

        //Calls the TaskOnClick/TaskWithParameters method when you click the Button
        btn1.onClick.AddListener(this.captureOnClick);
    }

    // Update is called once per frame
    void Update()
    {
        if (Animate) { 
        if (!temtem.transform.position.Equals(captureImage.transform.position))
        {
            captureImage.transform.position = Vector3.MoveTowards(captureImage.transform.position, temtem.transform.position, 0.1f);
        } 
        else
         {
                captureImage.transform.position = new Vector3(captureImage.transform.position.x, captureImage.transform.position.y, captureImage.transform.position.z - 5f);
                Animate = false;

            }
        }
    }

    void captureOnClick()
    {
        captureImage.SetActive(true);
        Dialog.SetActive(true);
        combatOptions.transform.position = new Vector3(combatOptions.transform.position.x, combatOptions.transform.position.y, combatOptions.transform.position.z + 100);
        StartCoroutine(setText2("YOU CAPTURED A NEW MEME!!", 3f));
        Animate = true;


    }

    IEnumerator setText(string text, float Time)
    {
            dMan.ShowBoxFull(text);
            yield return new WaitForSeconds(Time);

    }
    IEnumerator setText2(string text, float Time)
    {
        yield return StartCoroutine(setText("TAMEMER USED PHOTOSHOP SKILLS…", 1f));
        dMan.ShowBoxFull(text);
        yield return new WaitForSeconds(Time);
        action.inventory = 1;
        action.isCombat = false;
        dMan.dialogActive = false;
        CameraMain.SetActive(true);
        CombatCamera.SetActive(false);
    }
}
