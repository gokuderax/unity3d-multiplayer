﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class combatDialogManager : MonoBehaviour {
    public GameObject dBox;
    public Text dText;

    public GameObject combatOptions;

    public bool dialogActive;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (dialogActive && Input.GetKeyDown(KeyCode.E) && dText.text != "")
        {
            dBox.SetActive(false);
            dialogActive = false;
            dText.text = "";
            combatOptions.SetActive(true);
        }
    }

    public void ShowBox(string dialog)
    {
        dText.text = "";
        dialogActive = true;
        dBox.SetActive(true);
        StartCoroutine(TypeText(dialog));
    }
    public void ShowBoxFull(string dialog)
    {
        dText.text = "";
        dialogActive = true;
        dBox.SetActive(true);
        dText.text=dialog;
    }
    IEnumerator TypeText(string message)
    {
        foreach (char letter in message.ToCharArray())
        {
            dText.text += letter;
            yield return 0;
            yield return new WaitForSeconds(0.01f);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                dText.text = message;
                break;
            }
        }
    }
}
