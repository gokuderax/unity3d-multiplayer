﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HousingDialog : MonoBehaviour {

    public string[] dialogue;
    public Sprite image;
    private DialogHousingManager dMan;

    // Use this for initialization
    void Start () {
        dMan = FindObjectOfType<DialogHousingManager>();

    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.name == "pj")
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                if (!dMan.dialogActive)
                {
                    dMan.dialogueLines = dialogue;
                    dMan.currentLine = 0;
                    dMan.ShowBox(image);
                }
            }
        }
    }
}
