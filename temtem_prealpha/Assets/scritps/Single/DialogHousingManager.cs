﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogHousingManager : MonoBehaviour {

    public GameObject dBox;
    public Text dText;
    public GameObject dImage;

    public bool dialogActive;

    public string[] dialogueLines;
    public int currentLine;

    private bool blockE;

    public Personaje player;
    public bool combat;
    private Network network;

    // Use this for initialization
    void Start () {


        StartCoroutine(wait());

    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(1f * Time.deltaTime);
        GameObject gameObject = GameObject.Find("SocketIOController");
         network = gameObject.GetComponent<Network>();
        // Destroy(GameObject.Find(""));
        Character Player = gameObject.GetComponent<Network>().currentPlayer;
        player = GameObject.Find(Player.playerName).GetComponentInChildren<Personaje>();

    }
    // Update is called once per frame
    void Update () {
        if (dialogActive && Input.GetKeyUp(KeyCode.E) )
        {
            if (!blockE)
            {
                // dBox.SetActive(false);
                // dialogActive = false;
                dText.text = "";
                currentLine++;

                if (currentLine <= dialogueLines.Length -1)
                {
                    StartCoroutine(TypeText(dialogueLines[currentLine]));
                }
                if (currentLine >= dialogueLines.Length)
                {
                    dBox.SetActive(false);
                    dialogActive = false;
                    dText.text = "";
                    player.blockMove = false;

                    if (combat == true)
                    {
                        network.enterCombatNPC(1);
                        combat = false;
                    }
                }

            }
        }
    }


    public void ShowBox(Sprite ruta)
    {
        /* 
         dText.text = "";
         dialogActive = true;
         dBox.SetActive(true);
         StartCoroutine(TypeText(dialog));
         */
        if (ruta == null)
            dImage.SetActive(false);
        dImage.GetComponent<Image>().sprite = ruta;
        dialogActive = true;
        dBox.SetActive(true);
        StartCoroutine(TypeText(dialogueLines[0]));
        

    }

    IEnumerator TypeText(string message)
    {
        Debug.Log(message);
        foreach (char letter in message.ToCharArray())
        {
            blockE = true;
            dText.text += letter;
            yield return new WaitForSeconds(0.001f);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                dText.text = message;
                blockE = false;
                break;
            }
        }
        blockE = false;
    }
}
