﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class npcDialog : MonoBehaviour {


    public string dialogue;
    public GameObject surprise;
    public GameObject npc;
    public GameObject npcMove;

    private DialogManager dMan;
    private actions action;
    private bool animated = false;
	// Use this for initialization
	void Start () {
        dMan = FindObjectOfType<DialogManager>();
        action = FindObjectOfType<actions>();
	}
	
	// Update is called once per frame
	void Update () {
        if (animated) {
            if (!npc.transform.position.Equals(npcMove.transform.position))
            {
                npc.transform.position = Vector3.MoveTowards(npc.transform.position, npcMove.transform.position, 0.1f);
            }
        }
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.name == "pj")
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                if (action.inventory == 0)
                {
                    dMan.ShowBox("We have deleted all your memes You need new memes if you want to get on the train. What are you going to do now? Muahahahahahahaha.");
                } else
                {
                    dMan.ShowBox("I see that you have new memes...Get ready to fight!!!!");                    
                    StartCoroutine(move(1f));
                   
                }
            }
        }
    }

    IEnumerator move(float time)
    {
        yield return new WaitForSeconds(time);
        surprise.SetActive(true);
        while (true)
        {
            npc.transform.position = Vector3.MoveTowards(npc.transform.position, npcMove.transform.position, 0.1f);
            yield return new WaitForSeconds(0.01f);
            if (npc.transform.position.Equals(npcMove.transform.position))
            {
                break;
            }
        }
        yield return 0;
        yield return new WaitForSeconds(time);
        surprise.SetActive(false);       
    }
}
