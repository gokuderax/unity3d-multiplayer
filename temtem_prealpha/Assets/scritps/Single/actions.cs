﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class actions : MonoBehaviour
{

    //move
    public Vector3 starPoint;
    public Vector3 endPoint;
    public float speed = 10;
    float increment = 0;
    public bool isMoving = false;
    private Rigidbody2D rigid2d;
    float moveHorizontal;
    float moveVertical;
    public int inventory = 0;
    public bool houseInventory;

    //combat
    public int walkCounter;
    public int walkCounter2;
    public bool isCombat;
    public bool isAnimated;

    //Cameras
    public GameObject CameraMain;
    public GameObject CombatCamera;
    public Camera housingCamera;

    //temtem
    public GameObject temtem;
    public GameObject moveTemtem;

    //UI
    private combatDialogManager dMan;
    public Text txtCombat;
    public Transform objeto;
    public bool inventario = false;
    public bool copiar;
    public bool poner;
    private GameObject clon;
    private Vector2 size = new Vector2(1920, 1080);
    private float left;
    private float top;
    private float leftPlus;
    private float width;
    private float height;

    // Use this for initialization
    void Start()
    {
        starPoint = transform.position;
        endPoint = transform.position;
        dMan = FindObjectOfType<combatDialogManager>();
        rigid2d = GetComponent<Rigidbody2D>();
        walkCounter2 = Random.Range(150, 300);
    }

    // Update is called once per frame
    void Update()
    {

        if (increment <= 1 && isMoving == true)
        {
            increment += speed / 100;
        }
        else
        {
            isMoving = false;
        }


        if (Input.GetKeyDown("i"))
        {
            inventario = true;
        }

        if (houseInventory)
        {
            if (!inventario && objeto != null)
            {
                Vector3 posRaton = Input.mousePosition;
                posRaton.z = Vector3.Distance(housingCamera.transform.position, transform.position);
                posRaton = housingCamera.ScreenToWorldPoint(posRaton);
                objeto.position = posRaton;
                RaycastHit2D hit = Physics2D.Raycast(posRaton, Vector3.forward, 50f);

                if (hit.collider != null)
                {
                    Debug.Log(hit.collider.gameObject.tag);
                    Debug.Log(hit.collider.gameObject.CompareTag("Untagged"));

                    if (hit.collider.gameObject.CompareTag("Untagged")==true)
                    {
                        poner = true;
                    }


                }
                else
                {
                    poner = true;
                    if (Input.GetKeyDown("mouse 0") && poner == true)
                    {
                        // clon = GameObject.Instantiate(objeto).gameObject;
                        // clon.transform.position = objeto.position;

                        destruir();
                        objeto = null;
                        copiar = false;
                    }
                }

                /*Vector2 origin = new Vector2(housingCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                             housingCamera.ScreenToWorldPoint(Input.mousePosition).y);
                RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward,50f);
                Debug.Log(hit.collider);
                Debug.DrawLine(origin, Vector3.forward, Color.white, 100f, false);
                if (hit.collider == null)
                {

                }
                // objeto.position = new Vector3(hit.point.x, hit.point.y, 0);

                /*if (hit.collider.gameObject.tag != "Objects")
                {
                    Debug.Log("Did Hit");

                            Debug.Log("Target Position: " + hit.collider.gameObject.transform.position);
                            poner = true;
                        }

                        if (Input.GetKeyDown("mouse 0") && poner == true)
                        {
                            clon = GameObject.Instantiate(objeto).gameObject;

                            destruir();
                            copiar = false;
                        }*/
            }
        }


        if (isMoving)
        {
            rigid2d.AddForce(endPoint * increment);
        }
        if (!isCombat)
        {

            if (!dMan.dialogActive)
            {
                //Store the current horizontal input in the float moveHorizontal.
                moveHorizontal = Input.GetAxis("Horizontal");

                //Store the current vertical input in the float moveVertical.
                moveVertical = Input.GetAxis("Vertical");


                if (moveHorizontal < 0)
                {
                    rigid2d.velocity = new Vector2(-speed, 0);
                    rigid2d.AddForce(new Vector2(-speed, 0));
                    calculatewalk();
                }
                if (moveHorizontal > 0)
                {
                    rigid2d.velocity = new Vector2(speed, 0);
                    rigid2d.AddForce(new Vector2(speed, 0));
                    calculatewalk();
                }
                if (moveVertical < 0)
                {
                    rigid2d.velocity = new Vector2(0, -speed);
                    rigid2d.AddForce(new Vector2(0, -speed));
                    calculatewalk();
                }

                if (moveVertical > 0)
                {
                    rigid2d.velocity = new Vector2(0, speed);
                    rigid2d.AddForce(new Vector2(0, speed));
                    calculatewalk();
                }

                if (moveVertical == 0 && moveHorizontal == 0)
                {
                    rigid2d.velocity = new Vector2(0, 0);
                }
            }
        }
        else
        {
            rigid2d.velocity = new Vector2(0, 0);
            if (isAnimated)
            {
                if (temtem.transform.position.Equals(CombatCamera.transform.GetChild(0).position))
                {
                    dMan.ShowBox("Wild Pigepig Apparead!!!");
                    isAnimated = false;
                }
                else
                {
                    temtem.transform.position = Vector3.MoveTowards(temtem.transform.position, CombatCamera.transform.GetChild(0).position, 0.2f);
                }
            }
            else
            {
            }
        }
    }

    void calculatewalk()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector3.forward);
        Debug.DrawRay(transform.position, -Vector3.forward, Color.yellow);
        if (hit.collider.gameObject.tag == "tallGrass")
        {
            walkCounter++;
        }


        if (walkCounter >= walkCounter2)
        {
            walkCounter2 = Random.Range(150, 300);
            walkCounter = 0;
            enterCombat();
        }
    }

    void enterCombat()
    {
        CameraMain.SetActive(false);
        CombatCamera.SetActive(true);
        isAnimated = true;
        isCombat = true;
    }

    void OnGUI()
    {
        left = Screen.width / size.x * 30f;
        top = Screen.height / size.y * 5f;
        leftPlus = Screen.width / size.x * 215f;
        width = Screen.width / size.x * 200f;
        height = Screen.width / size.x * 70f;
        if (inventario)
        {
            if (GUI.Button(new Rect(left + leftPlus * 7.8f, top * 15, width,height), "Table"))
            {
                inventario = false;
                objeto = GameObject.Find("table").transform;
                clon = null;
                copiar = true;

            }


            if (GUI.Button(new Rect(left + leftPlus * 7.8f, top, width, height), "Computer"))
            {
                inventario = false;
                objeto = GameObject.Find("computer").transform;
                copiar = true;
                clon = null;
            }

            if (Input.GetKeyUp("i"))
            {
                inventario = false;
                clon = null;
            }
        }
    }

    void destruir()
    {
        if (copiar == false)
        {
            Destroy(clon);
            copiar = false;
        }
    }
}
