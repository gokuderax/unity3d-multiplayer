﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class housing : MonoBehaviour {

    public GameObject Fin;
    public GameObject Player;
    public GameObject cameraMain;
    public GameObject cameraHousing;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.name == "pj")
        {
            Player.transform.position = new Vector3(Fin.transform.position.x, Fin.transform.position.y, 62.8f);
            Player.transform.localScale = new Vector3(5, 5, 0);
            Player.GetComponent<actions>().speed = 8;
            Player.GetComponent<actions>().houseInventory = true;
            cameraMain.SetActive(false);
            cameraHousing.SetActive(true);
        }
    }
}
