﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LoginPanel : MonoBehaviour {
    public GameObject socketIO;

	private Network socket;
	private GameObject panelNetwork;

	private Canvas canvas;
	private Button btnRegistro;
	private Button btnLogin;
	private InputField usuario;
	private InputField contraseña;

	private GameObject panelLogin;
	private Transform panelRegistro;
	private Transform panelSalas;

	Dictionary<string, string> data;
	// Use this for initialization
	void Start () {
		canvas = GameObject.Find ("Canvas").GetComponent<Canvas> ();
		panelNetwork = GameObject.Find ("SocketIO");
		panelLogin = GameObject.Find ("LoginPanel");
		panelRegistro = canvas.GetComponentInChildren(typeof(Transform)).transform.Find ("RegistroPanel");
		panelSalas = canvas.GetComponentInChildren(typeof(Transform)).transform.Find ("SalasPanel");

		btnRegistro = GameObject.Find("btnRegistro").GetComponent<Button> ();
		btnLogin=GameObject.Find ("btnLogin").GetComponent<Button> ();
		usuario = GameObject.Find ("txtUsuario").GetComponent<InputField> ();
		contraseña = GameObject.Find ("txtContraseña").GetComponent<InputField> ();
		btnLogin.onClick.AddListener(OnclickBtnLogin);
		btnRegistro.onClick.AddListener (OnclickBtnRegistro);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnclickBtnRegistro()
	{
		
		panelRegistro.gameObject.SetActive (true);
		panelRegistro.transform.SetParent (canvas.transform);
		panelRegistro.transform.position = panelLogin.transform.position;
		panelLogin.SetActive (false);
	}

	void OnclickBtnLogin()
	{
		data= new Dictionary<string, string>();
		data["usuario"] = usuario.text;
		data ["password"] = contraseña.text;//BCrypt.Net.BCrypt.HashPassword (contraseña.text, BCrypt.Net.BCrypt.GenerateSalt (12));
		Debug.Log (BCrypt.Net.BCrypt.HashString (data ["password"]).ToString());
        //socketIO.GetComponent<Network>().Login(data);
        socket =socketIO.GetComponent<Network>();
        socket.Login(data);
        Debug.Log(new JSONObject(data));


        //socketIO..Login(data);
    }


}
