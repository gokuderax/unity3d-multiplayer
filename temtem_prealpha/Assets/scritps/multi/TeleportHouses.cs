﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportHouses : MonoBehaviour {


    public float[] destination = new float[2];

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                other.transform.position = new Vector3(destination[0],destination[1],0);
            }
        }
    }
}
