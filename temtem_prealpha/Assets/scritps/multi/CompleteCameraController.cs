﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CompleteCameraController : MonoBehaviour
{

    public GameObject player;       //Public variable to store a reference to the player game object

    private Vector3 offset;         //Private variable to store the offset distance between the player and camera


    private Network network;
    public Character playerInfo;

    // Use this for initialization
    void Start()
    {

        StartCoroutine(wait());

    }


    IEnumerator wait()
    {
      yield return new WaitForSeconds(0.01f);
     /*   GameObject gameObject = GameObject.Find("SocketIOController");
        network = gameObject.GetComponent<Network>();
        // Destroy(GameObject.Find(""));
        playerInfo = gameObject.GetComponent<Network>().currentPlayer;

        player = GameObject.Find(playerInfo.playerName).transform.GetChild(0).gameObject;
        transform.position = new Vector3(player.transform.position.x,player.transform.position.y,-10);*/
    }
    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
            if(player != null)
            transform.position = player.transform.position + offset;
       

        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        /* if (!colision)
         {
         } else
         {
             offset = transform.position - player.transform.position;
         }*/
    }

    public void setTarget(GameObject target)
    {
        player = target;
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
        offset = transform.position - player.transform.position;


    }

}
