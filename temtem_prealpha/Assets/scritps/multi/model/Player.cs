﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player {

    public string pos;
    public Vector3 position;
    public string animation;
    public bool firstTime;
    public bool hasChancla;
    public List<TemTem> temtems;
         
}

[Serializable]
public class TemTem
{
    public string temtemName;
    public string[] attacks;


    public TemTem(string v1, string[] v2)
    {
        this.temtemName = v1;
        this.attacks = v2;
    }
}
