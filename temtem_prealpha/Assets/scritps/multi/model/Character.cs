﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Character
{


    public string _id;
    public string playerName;
    public string password;
    public string email;
    public Player Player;
    public string id;

}

[System.Serializable]
public class Characters
{
    public List<Character> items;

    public static Characters CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<Characters>(jsonString);
    }
}