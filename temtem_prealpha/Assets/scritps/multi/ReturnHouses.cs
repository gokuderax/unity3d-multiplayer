﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnHouses : MonoBehaviour {


    public GameObject returnHouse;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {          
                other.transform.position = new Vector3(returnHouse.transform.position.x,returnHouse.transform.position.y,0);          
        }
    }
}
