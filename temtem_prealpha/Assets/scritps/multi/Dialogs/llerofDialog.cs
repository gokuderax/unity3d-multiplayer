﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class llerofDialog : MonoBehaviour {

    public string[] dialogue;
    public Sprite image;
    private DialogHousingManager dMan;

    // Use this for initialization
    void Start()
    {
        dMan = FindObjectOfType<DialogHousingManager>();

        dialogue = new string[1];
        dialogue[0] = "¡Vaya este juego esta genial! Deberias jugarlo se llama Digimon world 3";
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Personaje player = other.GetComponent<Personaje>();
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (!dMan.dialogActive)
                {
                    dMan.dialogueLines = dialogue;
                    dMan.ShowBox(image);
                    dMan.currentLine = 0;

                }
                else
                {

                    if (dMan.currentLine >= dMan.dialogueLines.Length - 1)
                    {
                        dMan.dBox.SetActive(false);
                        dMan.dialogActive = false;
                        dMan.dText.text = "";

                    }
                }

            }
        }
    }
}
