﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shuniDialog : MonoBehaviour {

    public string[] dialogue;
    public Sprite image;
    private DialogHousingManager dMan;

    // Use this for initialization
    void Start()
    {
        dMan = FindObjectOfType<DialogHousingManager>();

        dialogue = new string[2];
        dialogue[0] = "Tranquilo Besugin tengo un plan maestro para traerte a la vida, pronto estaremos juntos.";
        dialogue[1] = "Se que estamos destinados para pasar la vida juntos y sere todo tuyo.";
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Personaje player = other.GetComponent<Personaje>();
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (!dMan.dialogActive)
                {
                    dMan.dialogueLines = dialogue;
                    dMan.ShowBox(image);
                    dMan.currentLine = 0;

                }
                else
                {

                    if (dMan.currentLine >= dMan.dialogueLines.Length)
                    {
                        dMan.dBox.SetActive(false);
                        dMan.dialogActive = false;
                        dMan.dText.text = "";

                    }
                }
                player.blockMove = true;
            }
        }
    }
}
