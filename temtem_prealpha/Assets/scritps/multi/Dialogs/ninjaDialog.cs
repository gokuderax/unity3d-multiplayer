﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ninjaDialog : MonoBehaviour {


    public string[] dialogue;
    public Sprite image;
    private DialogHousingManager dMan;

    // Use this for initialization
    void Start()
    {
        dMan = FindObjectOfType<DialogHousingManager>();

        dialogue = new string[1];
        dialogue[0] = "Necesitas un meme para pasar por aqui!";

    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Personaje player = other.GetComponent<Personaje>();

            if (!player.Player.Player.hasChancla)
            {
                dialogue[0] = "Necesitas un meme para pasar por aqui!";
                other.transform.position = new Vector3(other.transform.position.x, other.transform.position.y - 1, other.transform.position.z);
                player.blockMove = true;
            }
            else if (player.Player.Player.hasChancla && player.Player.Player.temtems.Count == 0)
            {
                dialogue[0] = "Necesitas un me... Oh vaya tienes una chancla bueno eso me vale, puedes pasar";
                player.blockMove = true;
            }
            else if (player.Player.Player.hasChancla && player.Player.Player.temtems.Count >= 1)
            {
                dialogue[0] = "Veo que ya tienes un meme. ¡Luchemos!";
                player.blockMove = true;
                dMan.combat = true;
            }
        
    

                if (!dMan.dialogActive)
                {
                    dMan.dialogueLines = dialogue;
                    dMan.ShowBox(image);
                    dMan.currentLine = 0;

                }
                else
                {

                    if (dMan.currentLine >= dMan.dialogueLines.Length - 1)
                    {
                        dMan.dBox.SetActive(false);
                        dMan.dialogActive = false;
                        dMan.dText.text = "";
           
                }

            }
        }
    }
}
