﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class yawDialog : MonoBehaviour {


    public string[] dialogue;
    public Sprite image;
    private DialogHousingManager dMan;

    // Use this for initialization
    void Start()
    {
        dMan = FindObjectOfType<DialogHousingManager>();

        dialogue = new string[5];
        dialogue[0] = "...";
        dialogue[1] = "...";
        dialogue[2] = "...";
        dialogue[3] = "Oh! lo siento estaba configurando los colores del discord. Asi que quieres un meme para poder capturarlos a todos.";
        dialogue[4] = "Pues lo siento no te puedo ayudar no me queda ningun meme, toma te doy esta chancla y tu ya te apañas como puedas.";

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Personaje player = other.GetComponent<Personaje>();
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (!dMan.dialogActive)
                {
                    dMan.dialogueLines = dialogue;
                    dMan.ShowBox(image);
                    dMan.currentLine = 0;

                }
                else
                {

                    if (dMan.currentLine >= dMan.dialogueLines.Length)
                    {
                        dMan.dBox.SetActive(false);
                        dMan.dialogActive = false;
                        dMan.dText.text = "";

                    } else
                    {
                        if (!player.Player.Player.hasChancla)
                        {
                            player.Player.Player.hasChancla = true;
                        }
                        else if(player.Player.Player.hasChancla)
                        {
                            dialogue = new string[1];
                            dialogue[0] = "lo siento ya no tengo nada mas que ofrecerte, ahora dejame los colores no se configuran solos";
                        }
                        else if (player.Player.Player.hasChancla && player.Player.Player.temtems.Count >= 1)
                        {
                            dialogue = new string[5];
                            dialogue[1] = "¿¡Como!?, ya veo que has conocido el enigmatico hombre que se dedica a banear todos los memes que se encuentra.";
                            dialogue[2] = "solo una persona te puede ayudar, rapido tienes que ir al pueblo vecino yo mientras tanto contactare con el y le informare de la situacion.";

                        }

                    }


                }
                player.blockMove = true;

            }
        }
    }
}
