﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Personaje : MonoBehaviour {

    private Vector3 offset;


    public Character Player; 
    public Characters listPlayers;

    public Animator animador;
    int speed = 2;
    float axisName;
    float axsName;
    public  Rigidbody2D rigi;
    public string Nombre { get; internal set; }
    public bool isLocal = true;
    private bool moveAux = false;

    private Network network;
    private Vector3 namePlatePos;

    //UI
    public bool blockMove =false;
    //combat
    public int walkCounter;
    public int walkCounter2;
    //Cameras
    public GameObject principalCamera;
    public GameObject CombatCamera;

    // Use this for initialization
    void Start () {
        GameObject gameObject = GameObject.Find("SocketIOController");
        network = gameObject.GetComponent<Network>();
        // Destroy(GameObject.Find(""));
        Player = gameObject.GetComponent<Network>().currentPlayer;
        listPlayers = gameObject.GetComponent<Network>().listPlayers;
        walkCounter2 = Random.Range(15, 50);

        StartCoroutine(wait());


    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(1f*Time.deltaTime);
        if (GameObject.Find(Player.playerName) != null)
        {
            GameObject[] playersGameObject = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject go in playersGameObject)
            {
                Debug.Log(go.name);
                if (go.name.Equals(""))
                {
                    Destroy(go);
                }
                else if (go.name.Equals(Player.playerName))
                {
                    go.GetComponentInChildren<Personaje>().isLocal = true;
                    Camera.main.GetComponent<CompleteCameraController>().setTarget(go.transform.GetChild(0).gameObject);
                    go.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = Player.playerName;
                }
                else if (go.name != name)
                {
                    go.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = go.name;
                }
            }
        }

    }
    // Update is called once per frame
    void Update () {
        if (!isLocal)
        {
            return;
        }

        if (!blockMove)
        {
            axisName = Input.GetAxis("Horizontal");
            rigi.velocity = Vector2.right * axisName * speed;
            axsName = Input.GetAxis("Vertical");
            rigi.velocity = new Vector2(rigi.velocity.x, axsName * speed);
            Player.Player.position = transform.position;
            if (axisName != 0 || axsName != 0)
            {
                network.Move(Player);
                calculatewalk();
            }
            if (Input.GetKey(KeyCode.LeftShift))
                speed = 5;
            if (Input.GetKeyUp(KeyCode.LeftShift))
                speed = 2;
            if (speed > 4)
            {
                if (Input.GetKey(KeyCode.LeftControl) == false && Input.GetKey(KeyCode.Space) == false)
                {
                    if (axsName == 0 && axisName == 0)
                    {
                        animador.SetTrigger("Quieto");
                        if (moveAux == true)
                        {
                            moveAux = false;
                            Player.Player.animation = "Quieto";
                            network.Move(Player);
                        }

                    }
                    else if (axisName > 0)
                    {
                        //     transform.Translate(Vector2.right * speed * Time.deltaTime);
                        animador.SetTrigger("CorrerE");
                        Player.Player.animation = "CorrerE";
                        moveAux = true;
                    }
                    else if (axisName < 0)
                    {
                        //    transform.Translate(-Vector2.right * speed * Time.deltaTime);
                        animador.SetTrigger("CorrerO");
                        Player.Player.animation = "CorrerO";
                        moveAux = true;
                    }
                    else if (axsName > 0)
                    {
                        //    transform.Translate(Vector2.up * speed * Time.deltaTime);               
                        animador.SetTrigger("CorrerN");
                        Player.Player.animation = "CorrerN";
                        moveAux = true;
                    }

                    else if (axsName < 0)
                    {
                        //   transform.Translate(-Vector2.up * speed * Time.deltaTime);
                        animador.SetTrigger("CorrerS");
                        Player.Player.animation = "CorrerS";
                        moveAux = true;
                    }
                }
            }

            if (speed <= 3)
            {
                if (axisName > 0)
                {
                    //   transform.Translate(Vector2.right * speed * Time.deltaTime);
                    animador.SetTrigger("AndarE");
                    Player.Player.animation = "AndarE";
                    moveAux = true;
                }
                else if (axisName < 0)
                {
                    //    transform.Translate(-Vector2.right * speed * Time.deltaTime);
                    animador.SetTrigger("AndarO");
                    Player.Player.animation = "AndarO";
                    moveAux = true;
                }
                else if (axsName > 0)
                {
                    //  transform.Translate(Vector2.up * speed * Time.deltaTime);
                    animador.SetTrigger("AndarN");
                    Player.Player.animation = "AndarN";
                    moveAux = true;
                }
                else if (axsName < 0)
                {
                    //   transform.Translate(-Vector2.up * speed * Time.deltaTime);
                    animador.SetTrigger("AndarS");
                    Player.Player.animation = "AndarS";
                    moveAux = true;
                }
                else
                {
                    animador.SetTrigger("Quieto");
                    if (moveAux == true)
                    {
                        moveAux = false;
                        Player.Player.animation = "Quieto";
                        network.Move(Player);
                    }
                }
            }
        } else
        {
            rigi.velocity = Vector2.right*0;
            animador.SetTrigger("Quieto");
        }


    }


    void calculatewalk()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector3.forward, LayerMask.NameToLayer("tallGrass"));
        Debug.DrawRay(transform.position, -Vector3.forward, Color.yellow);
        if (hit.collider.gameObject.tag == "tallGrass")
        {
            walkCounter++;
        }


        if (walkCounter >= walkCounter2)
        {
            walkCounter2 = Random.Range(15, 50);
            walkCounter = 0;
            blockMove = true;
            network.enterCombatTemtem("route1");
        }
    }

    void enterCombat()
    {

    }

    /*
      void OnGUI()
      {
          // Place the name plate where the gameObject (player prefab) is
          if (isLocal)
          {
              GUIStyle namePlate;
              namePlatePos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
              GUI.Label(new Rect((namePlatePos.x - 30), (Screen.height - namePlatePos.y - 50), 100, 50), Player.playerName);
          } else
          {
              if (GameObject.Find(Player.playerName) != null)
              {
                  GameObject[] playersGameObject = GameObject.FindGameObjectsWithTag("Player");
                  foreach (GameObject go in playersGameObject)
                  {
                      if (!go.name.Equals(Player.playerName))
                      {
                          GUI.Label(new Rect((go.transform.position.x - 30), (Screen.height - go.transform.position.y - 50), 100, 50), go.GetComponent<Personaje>().Player.playerName);

                      }
                  }
              }
          }

      }

      void OnCollisionEnter2D(Collision2D col)
      {
          GameObject gameControllerObject = GameObject.FindWithTag("MainCamera");
          if (gameControllerObject != null)
          {
              cam = gameControllerObject.GetComponent<CompleteCameraController>();
          }
          if (cam == null)
          {
              Debug.Log("Cannot find 'GameController' script");
          }
          cam.colision = true;
         // camera.colision = true;   
      }
      void OnCollisionExit2D(Collision2D col)
      {
          GameObject gameControllerObject = GameObject.FindWithTag("MainCamera");
          if (gameControllerObject != null)
          {
              cam = gameControllerObject.GetComponent<CompleteCameraController>();
          }
          if (cam == null)
          {
              Debug.Log("Cannot find 'GameController' script");
          }
          cam.colision = false;
      }*/
}
