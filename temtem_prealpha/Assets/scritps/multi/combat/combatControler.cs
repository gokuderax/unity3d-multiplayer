﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class combatControler : MonoBehaviour {

    public GameObject panel;
    public Button attackPanel;

    internal bool myTurn = true;
	// Use this for initialization
	void Start () {

        attackPanel.onClick.AddListener(AttackPanel);
    }

    // Update is called once per frame
    void Update () {
    }

   public void AttackPanel()
    {
        if (myTurn)
        {

            if (panel.activeInHierarchy == true)
                panel.SetActive(false);
            else 
                panel.SetActive(true);

        }
    }

    
}
